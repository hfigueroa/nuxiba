/*Con esta consulta se extrae la llamadas de febrero ya que nos especifica el año en el requerimiento*/
select * from logdial where tipodellamada = 'Cel LD'
	and extract(month from fechadellamada) = 2

/*Ya que solo hay registros del 2020 se ajusta a este año*/
select * from logdial where tipodellamada = 'Cel LD'
	and fechadellamada::date between '2020-02-01' and  '2020-02-28'