select tmp_suma.tipodellamada, tmp_suma.tiempodialogo, tmp_no.num_registros
	, (tmp_suma.tiempodialogo / tmp_no.num_registros) as promedio
from (
	select tipodellamada, sum(tiempodialogo)::integer as tiempodialogo
	from logdial 
	where tipodellamada = 'Cel LD'
		and extract(month from fechadellamada) = 2
	group by tipodellamada
) tmp_suma
inner join (
	select tipodellamada, count(tiempodialogo)::integer as num_registros
	from logdial 
	where tipodellamada = 'Cel LD'
		and extract(month from fechadellamada) = 2
	group by tipodellamada
) tmp_no on tmp_suma.tipodellamada = tmp_no.tipodellamada