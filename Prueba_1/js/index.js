(function(){

	var userIDSelected;
	var lenghtComments = 0;
	var optContinue = true;

	function Index() {
		funViewListUser();
		
		$("#btn_back").on("click", function(){ funBack(); });

		$("#btn_posts").on("click", function(){ funGetPosts(); });
		$("#btn_todos").on("click", function(){ funGetTodos(); });
		$("#btn_save").on("click", function(){ funSave(); });
	}

	var funGetData = function(pUrl, pCallback) {

		$("#div_loading").show();

        fetch(pUrl)
        	.then((resp) => resp.json())
        	.then(function(data){
        		
        		$("#div_loading").hide();

        		if (pCallback) {
        			pCallback(data);
        		}
        	})
        	.catch(function(){

        		$("#div_loading").hide();
        		alert("Algo salio mal, intente mas tarde.");

        	});
	}

	var funPostData = function(pUrl, pParams, pCallback) {

		$("#div_loading").show();

		fetch(pUrl, {
				method: 'POST',
				body: JSON.stringify(pParams),
				headers:{
					'Content-Type': 'application/json'
				},
			})
			.then(res => res.json())
			.then(function(data){
        		
        		$("#div_loading").hide();

        		if (pCallback) {
        			pCallback(data);
        		}

        	})
        	.catch(function(){

        		$("#div_loading").hide();
        		alert("Algo salio mal, intente mas tarde.");

        	});
	}

	var funViewListUser = function(){

		funGetData("https://jsonplaceholder.typicode.com/users", function(pUsers){

			var html = "";
			for (var idx in pUsers ) {
				user = pUsers[idx];

				html += "<tr id='tr_user_" + user.id + "' user_id='" + user.id + "'>" +
						"<td>" + user.name + "</td>" +
						"<td>" + user.username + "</td>" +
						"<td>" + user.email + "</td>" +
					"</tr>";
			}

			$("#tbl_users tbody").html(html);
			$("#tbl_users tbody tr").on("click", function(){funUserSelect(this);});
			$("#tbl_users tbody tr")
				.on("mouseover", function(){
					$(this).addClass("hight-light");
				})
				.on("mouseout",function(){
					$(this).removeClass("hight-light");
				});
		});


	}

	var funUserSelect = function(pUser) {

		funCleanView();

		$("#tbl_users tbody tr").removeClass("user-selected");

		$(pUser).addClass("user-selected");

		userIDSelected = $(pUser).attr("user_id");

		$("#div_user").removeClass("form-hide");
		$("#div_user_list").addClass("form-hide");

		funGetData("https://jsonplaceholder.typicode.com/users/" + userIDSelected, function(pUserData){
			$("#div_name span").html("<b>" + pUserData.name + "</b>");
			$("#div_address span").html("<b>Street " + pUserData.address.street 
					+ ",<br> suite " + pUserData.address.suite
					+ ",<br> city " + pUserData.address.city 
					+ ",<br> zipcode " + pUserData.address.zipcode + "</b>");
			$("#div_username span").html("<b>" + pUserData.username + "</b>");
			$("#div_mail span").html("<b>" + pUserData.email + "</b>");
			$("#div_phone span").html("<b>" + pUserData.phone + "</b>");
			$("#div_web span").html("<b>" + pUserData.website + "</b>");
			$("#div_company span").html("<b>" + pUserData.company.name 
					+ ",<br> frase " + pUserData.company.catchPhrase 
					+ ",<br> giro " + pUserData.company.bs + "</b>");
		});
	}

	var funBack = function(){
		$("#div_user").addClass("form-hide");
		$("#div_user_list").removeClass("form-hide");

		funCleanView();
	}

	var funCleanView = function() {
		$("#div_todos_content, #div_posts_content").html("");
		$("#div_posts, #div_todos").addClass("form-hide");

		funCleanField();
	}

	var funCleanField = function() {
		$("#txt_title").val("");
		$("#chk_completed").prop("checked", false);
	}

	var funGetPosts = function(){

		$("#div_todos").addClass("form-hide");
		$("#div_posts").removeClass("form-hide");

		funGetData("_api/get_posts.php?userid=" + userIDSelected, function(pPosts){

			var html = "";

			for (var idx in pPosts) {
				html += "<div class='post'>"
					+ "<b>" + pPosts[idx].title + "</b><br/><br/>"
					+  "<span>" + pPosts[idx].body + "</span>";

				html += "<div class='comments'>"
				for (idxComment in pPosts[idx].comments) {
					html += "<b>Comentario " + (idxComment * 1 + 1) + "</b><br/>"
						+ "<div class='left'>Autor: " + pPosts[idx].comments[idxComment].email + "</div>"
						+ "<div class='left'>Nombre:" + pPosts[idx].comments[idxComment].name + "</div>"
						+ "<div class='left'>Comentario:" + pPosts[idx].comments[idxComment].body + "</div>";
				}

				html += "</div>";

				html +=	 "<hr>"
					+ "</div>";
			}

			$("#div_posts_content").html(html);
			

			
		});
		
	}

	var funGetTodos = function(){

		$("#div_posts").addClass("form-hide");
		$("#div_todos").removeClass("form-hide");

		funGetData("https://jsonplaceholder.typicode.com/users/"  + userIDSelected + "/todos", function(pTodos) {
			
			var html = "";

			todos = pTodos.slice(0);

			todos.sort(function(a,b) {
				return a.id - b.id;
			});

			for (var idx in todos) {
				var todo = todos[idx];

				html += "<div class='todo " + (todo.completed ? "todo-completed" : "todo-uncompleted") + "'>"
						+ "<span>ID: " + todo.id + " - " + (todo.completed ? "Completada" : "Sin completar" ) + "</span><br/>"
						+ "<span>" + todo.title + "</span>"
					+ "</div>";
			}

			$("#div_todos_content").html(html);
		});
	}

	var funSave = function() {

		var title = $("#txt_title").val();

		if (!title) {
			alert("No se ha indicado el titulo de la tarea.");
			return;
		}

		var params = {
			title: title,
			opt_completed: ( $("#chk_completed").is(":checked") ? true : false)
		}

		funPostData("https://jsonplaceholder.typicode.com/users/" + userIDSelected + "/todos", params,  function(){
			funCleanField();
			alert("Se guardo correctamente la tarea.");
		})
	}

	Index.prototype = {
		//users: funListUser
	}

	window.Index = Index;

})()

$(document).ready(function(){
	var oIndex = new window.Index();
})