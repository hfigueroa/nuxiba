<?php


$fullPost = getPosts();

header('Content-Type: application/json');
echo json_encode($fullPost);
exit;


function getPosts() {
	$userID = $_GET["userid"];

	$urlPost = "https://jsonplaceholder.typicode.com/users/$userID/posts";

	$posts = doRequest($urlPost);

	for ($i=0; $i < count($posts); $i++) { 
	 	$urlComments = "https://jsonplaceholder.typicode.com/post/" . $posts[$i]->{"id"} . "/comments";
	 	$posts[$i]->{"comments"} = doRequest($urlComments);
	 }

	 return $posts; 
}

function getComments($pUrlComments){

}

function doRequest($pUrl) {
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $pUrl);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$output = json_decode(curl_exec($ch));
	curl_close($ch);

	return $output;
}