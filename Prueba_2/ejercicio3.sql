select sum(num_min) as num_min_mes, sum(costo) as costo
from (
	select (tiempodialogo / 60)::float as num_min, costo
	from logdial l
	inner join costos c on c.tipodellamada = l.tipodellamada
	where extract(month from fechadellamada) = 1
) tmp